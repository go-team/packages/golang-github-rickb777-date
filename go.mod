module github.com/rickb777/date

require (
	github.com/onsi/gomega v1.24.0
	github.com/rickb777/plural v1.4.1
	golang.org/x/text v0.4.0
)

require (
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/mattn/goveralls v0.0.11 // indirect
	github.com/sqs/goreturns v0.0.0-20181028201513-538ac6014518 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/net v0.1.0 // indirect
	golang.org/x/sys v0.1.0 // indirect
	golang.org/x/tools v0.1.12 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

go 1.17
